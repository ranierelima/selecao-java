package br.com.ranieredelima.desafioindraweb.config;

import java.util.Optional;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.com.ranieredelima.desafioindrabase.model.Usuario;
import br.com.ranieredelima.desafioindrabase.repository.UsuarioRepository;

@Configuration
public class ApplicationConfiguration {
	
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	public CommandLineRunner loadData(UsuarioRepository repository) {
	    return (args) -> {
	    	
	    	// Criando usuário padrão
	    	// E-mail: admin@admin.com
	    	// Senha: 123456
	    	Optional<Usuario> find = repository.findById(1L);
	    	if( !find.isPresent() ) {
	    		Usuario usuario = new Usuario();
	    		usuario.setId(1L);
	    		usuario.setEmail("admin@admin.com");
	    		usuario.setNome("Administrador do Sistema");
	    		usuario.setSenha("$2a$10$3fleShhlgQt5RAT7Bqj7Ae.r1qBo8dUy/RNShifPS6/i6X13P6RK2");

	    		Usuario save = repository.save(usuario);
	    		System.out.println(save == null ? "Erro ao salvar usuario" : "Usuario salvo com sucesso");
	       }
	    };
	}

}
