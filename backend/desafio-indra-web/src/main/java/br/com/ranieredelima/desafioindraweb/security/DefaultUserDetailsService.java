package br.com.ranieredelima.desafioindraweb.security;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class DefaultUserDetailsService implements UserDetailsService {

	private final List<SimpleGrantedAuthority> authorities = Arrays.asList(new SimpleGrantedAuthority( "USER" ));
	
	private static final Logger logger = LoggerFactory.getLogger(DefaultUserDetailsService.class);

	private static final String USER_SQL = "select id, email, senha from usuario where email = ?";

	@Autowired
	private DataSource dataSource;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
		logger.debug("Looking for {}", username);

		try (Connection conx = dataSource.getConnection(); PreparedStatement ps = conx.prepareStatement(getUserSQL())) {

			ps.setString(1, username);
			try (ResultSet rs = ps.executeQuery()) {
				if (!rs.next()) {
					throw new UsernameNotFoundException("Username not found: " + username);
				}

				return createUserDetails(rs);
			}
		} catch (SQLException e) {
			throw new UsernameNotFoundException("Error when trying to retrieve the user " + username, e);
		}
	}

	protected UserDetails createUserDetails(ResultSet rs) throws SQLException {
		return new AppUser(rs.getLong(1), rs.getString(2), rs.getString(3), authorities);
	}

	protected String getUserSQL() {
		return USER_SQL;
	}
}