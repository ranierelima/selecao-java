package br.com.ranieredelima.desafioindraweb.controller.generic;

public abstract class GenericMessage {
	
	private String message;
	
	public GenericMessage(String message){
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
