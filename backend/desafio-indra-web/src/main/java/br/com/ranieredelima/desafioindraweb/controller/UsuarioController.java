package br.com.ranieredelima.desafioindraweb.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.ranieredelima.desafioindrabase.model.Usuario;
import br.com.ranieredelima.desafioindrabase.service.UsuarioService;
import br.com.ranieredelima.desafioindraweb.controller.generic.BaseController;
import br.com.ranieredelima.desafioindraweb.controller.generic.GenericMessage;

@RestController
@RequestMapping("/usuario")
public class UsuarioController extends BaseController<Usuario> {

	private static final String DEFAULT_PASS = "123456";
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	public UsuarioController(UsuarioService service, BCryptPasswordEncoder bCryptPasswordEncoder) {
		super(service);
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	@Override
	public GenericMessage save(@Valid @RequestBody Usuario model, BindingResult bindingResult, HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		if (model != null) {
			String pass = model.getSenha();

			if (pass == null || pass.isEmpty()) {

				pass = DEFAULT_PASS;
			}

			model.setSenha(bCryptPasswordEncoder.encode(pass));
		}
		
		return super.save(model, bindingResult, request, response);
	}

}
