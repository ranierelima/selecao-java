package br.com.ranieredelima.desafioindraweb.config;

import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import br.com.ranieredelima.desafioindraweb.filter.JWTAuthenticationFilter;
import br.com.ranieredelima.desafioindraweb.filter.JWTAuthorizationFilter;
import br.com.ranieredelima.desafioindraweb.security.DefaultUserDetailsService;
import br.com.ranieredelima.desafioindraweb.security.SecurityConstants;

@EnableWebSecurity
public class AuthConfiguration extends WebSecurityConfigurerAdapter {

	private DefaultUserDetailsService userDetailsService;
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public AuthConfiguration(DefaultUserDetailsService userDetailsService,
			BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.userDetailsService = userDetailsService;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().and().csrf().disable().authorizeRequests()
        .antMatchers(
    		HttpMethod.GET, 
    		SecurityConstants.SWAGGER_URL, 
    		SecurityConstants.SWAGGER_HTML_FILE, 
    		SecurityConstants.SWAGGER_WEBJARS,
    		SecurityConstants.SWAGGER_RESOURCES
        ).permitAll()
        .anyRequest().authenticated().and()
		.addFilter(new JWTAuthenticationFilter(authenticationManager()))
		.addFilter(new JWTAuthorizationFilter(authenticationManager(), userDetailsService))
		// this disables session creation on Spring Security
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
	}

	@Bean
	CorsConfigurationSource corsConfigurationSource() {
		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
		return source;
	}
	
}
