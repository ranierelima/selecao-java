package br.com.ranieredelima.desafioindraweb.security;

public class SecurityConstants {
	private SecurityConstants() {}
	
	public static final String SECRET = "PrkDRTkoOtLeqPF0";
    public static final long EXPIRATION_TIME = 36000000; // 10 horas
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "x-access-token";
    public static final String SWAGGER_URL = "/v2/api-docs";
    public static final String SWAGGER_HTML_FILE = "/swagger-ui.html";
    public static final String SWAGGER_WEBJARS = "/webjars/**";
    public static final String SWAGGER_RESOURCES = "/swagger-resources/**";
}
