package br.com.ranieredelima.desafioindraweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesafioIndraWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioIndraWebApplication.class, args);
	}

}

