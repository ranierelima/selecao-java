package br.com.ranieredelima.desafioindraweb.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.ranieredelima.desafioindrabase.dto.FileImportCSV;
import br.com.ranieredelima.desafioindrabase.dto.ValorMedioDTO;
import br.com.ranieredelima.desafioindrabase.model.Historico;
import br.com.ranieredelima.desafioindrabase.model.Regiao;
import br.com.ranieredelima.desafioindrabase.service.HistoricoService;
import br.com.ranieredelima.desafioindraweb.controller.generic.BaseController;
import br.com.ranieredelima.desafioindraweb.controller.generic.GenericMessage;
import br.com.ranieredelima.desafioindraweb.controller.generic.GenericMessageErrorDTO;
import br.com.ranieredelima.desafioindraweb.controller.generic.GenericMessageSuccessDTO;

@RestController
@RequestMapping("/historico")
public class HistoricoController extends BaseController<Historico> {

	@Autowired
	public HistoricoController(HistoricoService service) {
		super(service);
	}

	@PostMapping("/importar")
	public GenericMessage importacaoArquivo(@RequestBody FileImportCSV fileImport, HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		if (fileImport != null) {

			GenericMessage result = null;

			if ( ((HistoricoService) service).importacaoArquivo(fileImport) ) {
				result = new GenericMessageSuccessDTO("Dados importados com sucesso");
			} else {
				result = new GenericMessageErrorDTO(411, "Ocorreu um erro durante a importação dos dados");
			}
			
			return result;
		}

		return new GenericMessageErrorDTO(404, "Dados não informados");
	}

	@GetMapping("/mediaCombustivelPorMunicipio")
	public GenericMessage obterMediaPrecoCombustivelPeloMunicipio(
		@RequestParam(value = "municipio", required = true) String municipio
	) {
		
		List<Object[]> media = ((HistoricoService) service).obterMediaPrecoCombustivelPeloMunicipio(municipio.toUpperCase());
		
		Map<Object, Object> result = new HashMap<>();

		if (media != null ) {
			
			for(Object[] m : media ) {
				result.put(m[0], m[1]);
			}
			return new GenericMessageSuccessDTO("Media de Combustivel do Municipio " + municipio.toUpperCase(), result);
		}
		
		return new GenericMessageErrorDTO(404, "Não foi possível obter a média deste municipio");
		
	}
	
	@GetMapping("/dadosPorRegiao")
	public GenericMessage obterDadosPelaRegiao(
		@RequestParam(value = "regiao", required = true) String regiao
	) {
		
		Regiao regiaoEnum = Regiao.valueOf(regiao);
		
		if (regiaoEnum == null) {
			return new GenericMessageErrorDTO(404, "Região informada não existe");
		}
		
		List<Historico> dados = ((HistoricoService) service).obterDadosPelaRegiao(regiaoEnum);
		
		if(dados == null || dados.isEmpty()) {
			
			return new GenericMessageErrorDTO(404, "Não foi possível obter os dados desta região");
		}
		
		return new GenericMessageSuccessDTO("Dados da região: " + regiaoEnum.getNome(), dados);
		
	}
	
	@GetMapping("/mediaCompraPorMunicipio")
	public GenericMessage obterMediaPrecoCompraPorMunicipio(
		@RequestParam(value = "municipio", required = true) String municipio
	) {
		
		ValorMedioDTO medioDTO = ((HistoricoService) service).getMediaPeloMunicipio(municipio.toUpperCase());
		
		if (medioDTO != null ) {
			
			return new GenericMessageSuccessDTO("Media de compras do Municipio " + municipio.toUpperCase(), medioDTO);
		}
		
		return new GenericMessageErrorDTO(404, "Não foi possível obter a média deste municipio");
		
	}

	@GetMapping("/mediaCompraPorBandeira")
	public GenericMessage obterMediaPrecoCompraPorBandeira(
		@RequestParam(value = "bandeira", required = true) String bandeira
	) {
		
		ValorMedioDTO medioDTO = ((HistoricoService) service).getMediaPelaBandeira(bandeira.toUpperCase());
		
		if (medioDTO != null ) {
			
			return new GenericMessageSuccessDTO("Media de compras do Municipio " + bandeira.toUpperCase(), medioDTO);
		}
		
		return new GenericMessageErrorDTO(404, "Não foi possível obter a média deste municipio");
		
	}

}
