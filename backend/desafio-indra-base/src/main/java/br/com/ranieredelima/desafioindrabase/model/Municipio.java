package br.com.ranieredelima.desafioindrabase.model;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.ranieredelima.desafioindrabase.model.convert.EstadoConvert;
import br.com.ranieredelima.desafioindrabase.model.generic.BaseModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "municipio")
@NoArgsConstructor
public class Municipio extends BaseModel {

	private static final long serialVersionUID = -1554023620909451162L;

	@Getter @Setter private String nome;
	
	@Convert(converter = EstadoConvert.class)
	@Getter @Setter private Estado estado;
}
