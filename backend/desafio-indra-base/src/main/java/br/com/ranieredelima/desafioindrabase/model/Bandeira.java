package br.com.ranieredelima.desafioindrabase.model;

import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.ranieredelima.desafioindrabase.model.generic.BaseModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "bandeira")
@NoArgsConstructor
public class Bandeira extends BaseModel{

	private static final long serialVersionUID = -5445894012326131419L;
	
	@Getter @Setter private String nome;
	
}
