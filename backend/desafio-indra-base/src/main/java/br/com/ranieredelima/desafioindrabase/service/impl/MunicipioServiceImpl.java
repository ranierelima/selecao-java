package br.com.ranieredelima.desafioindrabase.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ranieredelima.desafioindrabase.model.Municipio;
import br.com.ranieredelima.desafioindrabase.repository.MunicipioRepository;
import br.com.ranieredelima.desafioindrabase.service.MunicipioService;

@Service
public class MunicipioServiceImpl extends BaseServiceImpl<Municipio> implements MunicipioService{

	@Autowired
	public MunicipioServiceImpl(MunicipioRepository repository) {
		super(repository);
	}
	
	@Override
	public Municipio findByNome(String nome) {
		return ( (MunicipioRepository) repository).findByNome(nome);
	}

}
