package br.com.ranieredelima.desafioindrabase.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ranieredelima.desafioindrabase.dto.FileImportCSV;
import br.com.ranieredelima.desafioindrabase.dto.ValorMedioDTO;
import br.com.ranieredelima.desafioindrabase.model.Bandeira;
import br.com.ranieredelima.desafioindrabase.model.Estado;
import br.com.ranieredelima.desafioindrabase.model.Historico;
import br.com.ranieredelima.desafioindrabase.model.Municipio;
import br.com.ranieredelima.desafioindrabase.model.Regiao;
import br.com.ranieredelima.desafioindrabase.repository.HistoricoRepository;
import br.com.ranieredelima.desafioindrabase.service.BandeiraService;
import br.com.ranieredelima.desafioindrabase.service.HistoricoService;
import br.com.ranieredelima.desafioindrabase.service.MunicipioService;

@Service
public class HistoricoServiceImpl extends BaseServiceImpl<Historico> implements HistoricoService{
	
	private MunicipioService municipioService;
	private BandeiraService bandeiraService;

	@Autowired
	public HistoricoServiceImpl(HistoricoRepository repository, MunicipioService municipioService,
			BandeiraService bandeiraService) {
		super(repository);
		this.municipioService = municipioService;
		this.bandeiraService = bandeiraService;
	}
	
	@Override
	public Historico save(Historico entity) {

		if (entity != null && entity.getMunicipio() != null && entity.getMunicipio().getId() == null) {
			Municipio municipio = municipioService.findByNome(entity.getMunicipio().getNome().toUpperCase());

			if (municipio == null) {
				entity.getMunicipio().setNome(entity.getMunicipio().getNome().toUpperCase());
				municipio = municipioService.save(entity.getMunicipio());
			} // if

			entity.setMunicipio(municipio);
		} // if

		if (entity != null && entity.getBandeira() != null && entity.getBandeira().getId() == null) {
			Bandeira bandeira = bandeiraService.findByNome(entity.getBandeira().getNome().toUpperCase());

			if (bandeira == null) {
				entity.getBandeira().setNome(entity.getBandeira().getNome().toUpperCase());
				bandeira = bandeiraService.save(entity.getBandeira());
			} // if

			entity.setBandeira(bandeira);
		} // if
		
		return super.save(entity);
	}

	@Override
	public Boolean importacaoArquivo(FileImportCSV fileImport) throws IOException {
		Boolean result = Boolean.FALSE;
		File arquivoCSV = null;
		BufferedReader br = null;
		
		try{
			arquivoCSV = obterArquivo(fileImport);
			br = new BufferedReader(new FileReader(arquivoCSV));

			String linha = null;
			
			int qtde = 0;
			
			while ((linha = br.readLine()) != null) {

				if(fileImport.getInicio() <= qtde) {
					
					String[] dados = linha.split("\\t");
					
					Municipio municipio = new Municipio();
					municipio.setEstado(Estado.findBySigla(dados[1]));
					municipio.setNome(dados[2]);
					
					Historico historico = new Historico();
					historico.setNomeEstabelecimento(dados[3]);
					historico.setProduto(dados[4]);
					historico.setData(new SimpleDateFormat("dd/MM/yyyy").parse(dados[5]));
					historico.setValorCompra( (dados[6] == null || dados[6].isEmpty()) ? null : Double.valueOf(dados[6].replace(",", ".")));
					historico.setValorVenda(Double.valueOf(dados[7].replace(",", ".")));
					historico.setUnidadeMedida(dados[8]);
					
					Bandeira bandeira = new Bandeira();
					bandeira.setNome(dados[9]);

					historico.setBandeira(bandeira);
					historico.setMunicipio(municipio);
					
					this.save(historico);
					
				}
				
				qtde++;
			}
			
			result = Boolean.TRUE;

		} catch (FileNotFoundException e) {
			logger.error("Arquivo não foi encontrado.", e);
		
		} catch (ParseException e) {
			logger.error("Ocorreu um erro ao converter a data." , e);
			
		} catch (IndexOutOfBoundsException e) {
			logger.error("Ocorreu um erro. O arquivo não está formatado como esperado" , e);
			
		} catch (Exception e) {
			logger.error("Ocorreu um erro.", e);

		} finally {
			FileUtils.deleteQuietly(arquivoCSV);
		}
		
		return result;
	}

	private File obterArquivo(FileImportCSV fileImport) throws IOException {
		String base64 = null;

		if (fileImport.getData().contains(":base64,")) {
			String[] split = fileImport.getData().split(";base64,");
			base64 = split[1];
		} else {
			base64 = fileImport.getData();
		}

		
		byte[] data = Base64.getDecoder().decode(base64);

		File arquivoCSV = new File("arquivo-importacao-" + Calendar.getInstance().getTimeInMillis() + ".csv");
		FileUtils.writeByteArrayToFile(arquivoCSV, data);
		return arquivoCSV;
	}

	@Override
	public List<Object[]> obterMediaPrecoCombustivelPeloMunicipio(String municipio) {
		return ((HistoricoRepository) repository).obterMediaPrecoPeloMunicipio(municipio);
	}

	@Override
	public List<Historico> obterDadosPelaRegiao(Regiao regiaoEnum) {
		return ((HistoricoRepository) repository).obterDadosPelaRegiao(regiaoEnum.findAllEstados());
	}

	@Override
	public ValorMedioDTO getMediaPeloMunicipio(String municipio) {
		return ((HistoricoRepository) repository).getMediaPeloMunicipio(municipio);
	}

	@Override
	public ValorMedioDTO getMediaPelaBandeira(String bandeira) {
		return ((HistoricoRepository) repository).getMediaPelaBandeira(bandeira);
	}
}
