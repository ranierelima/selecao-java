package br.com.ranieredelima.desafioindrabase.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class AuthenticationDTO {

	@Getter @Setter private String username;
	@Getter @Setter private String password;

}
