package br.com.ranieredelima.desafioindrabase.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class FileImportCSV {

	@Getter @Setter private String name;
	@Getter @Setter private String data;
	@Getter @Setter private int inicio;
}
