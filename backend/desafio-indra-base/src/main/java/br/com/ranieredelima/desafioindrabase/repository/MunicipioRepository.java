package br.com.ranieredelima.desafioindrabase.repository;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.ranieredelima.desafioindrabase.model.Municipio;

@Repository
public interface MunicipioRepository extends BaseRepository<Municipio>{

	Municipio findByNome(@Param("nome") String nome);

}
