package br.com.ranieredelima.desafioindrabase.repository;

import org.springframework.stereotype.Repository;

import br.com.ranieredelima.desafioindrabase.model.Usuario;

@Repository
public interface UsuarioRepository extends BaseRepository<Usuario>{

}
