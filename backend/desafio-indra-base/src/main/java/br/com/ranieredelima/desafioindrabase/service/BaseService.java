package br.com.ranieredelima.desafioindrabase.service;

import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.data.domain.Page;
import org.springframework.lang.Nullable;

import br.com.ranieredelima.desafioindrabase.model.generic.BaseModel;


public interface BaseService <T extends BaseModel>{
	
	void delete(Long id) throws EntityNotFoundException;
	Optional<T> get(Long id);
	Page<T> list(Integer page, @Nullable String orderBy, @Nullable String direction);
	T save(T entity);

}