package br.com.ranieredelima.desafioindrabase.service;

import br.com.ranieredelima.desafioindrabase.model.Municipio;

public interface MunicipioService extends BaseService<Municipio>{

	Municipio findByNome(String nome);

}
