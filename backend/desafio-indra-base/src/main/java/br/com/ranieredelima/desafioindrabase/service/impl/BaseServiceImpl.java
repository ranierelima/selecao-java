package br.com.ranieredelima.desafioindrabase.service.impl;

import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.lang.Nullable;

import br.com.ranieredelima.desafioindrabase.model.generic.BaseModel;
import br.com.ranieredelima.desafioindrabase.repository.BaseRepository;
import br.com.ranieredelima.desafioindrabase.service.BaseService;

public class BaseServiceImpl<T extends BaseModel> implements BaseService<T>{

	protected static final String DEFAULT_SORT = "id";

	protected static final int PAGE_SIZE = 15;

	protected BaseRepository<T> repository;
	
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	public BaseServiceImpl(BaseRepository<T> repository) {
		this.repository = repository;
	}
	
	@Override
	public void delete(Long id) throws EntityNotFoundException {
		Optional<T> object = get(id);

		if (!object.isPresent()) {
			String msgError = "Não foi possível localizar a entidade do id " + id;
			
			logger.error(msgError);
			throw new EntityNotFoundException(msgError);
		} // if
		
		this.repository.deleteById(id);		
		
	} // delete()

	@Override
	public Optional<T> get(Long id) {
		return this.repository.findById(id);
	} // get()

	@Override
	public Page<T> list(Integer page, @Nullable String orderBy, @Nullable String direction) {
		Sort sort = (orderBy != null && !orderBy.isEmpty()) ? getSort(DEFAULT_SORT) : getSort(orderBy);

		sort = (direction != null && direction.equalsIgnoreCase("Desc")) ? sort.descending() : sort.ascending() ;
		
		PageRequest request = PageRequest.of(Optional.ofNullable(page).orElse(0), PAGE_SIZE, sort);
		return this.repository.findAll(request);
	} // list()

	protected Sort getSort(String orderBy) {
		return Sort.by( Order.by(orderBy) );
	} // getDefaultSort()

	@Override
	public T save(T entity) {
		
		if(entity != null) {
			return this.repository.save(entity);
		} // if
		
		logger.error("Não é possível saber um objeto nulo");
		return null;
	} // save()
	
}