package br.com.ranieredelima.desafioindrabase.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ranieredelima.desafioindrabase.model.Bandeira;
import br.com.ranieredelima.desafioindrabase.repository.BandeiraRepository;
import br.com.ranieredelima.desafioindrabase.service.BandeiraService;

@Service
public class BandeiraServiceImpl extends BaseServiceImpl<Bandeira> implements BandeiraService{

	@Autowired
	public BandeiraServiceImpl(BandeiraRepository repository) {
		super(repository);
	}

	@Override
	public Bandeira findByNome(String nome) {
		return ( (BandeiraRepository) repository).findByNome(nome);
	}

}
