package br.com.ranieredelima.desafioindrabase.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

public enum Estado {

	AC("Acre", "AC", Regiao.NORTE), 
	AL("Alagoas", "AL", Regiao.NORDESTE), 
	AP("Amapá", "AP", Regiao.NORTE), 
	AM("Amazonas", "AM", Regiao.NORTE), 
	BA("Bahia", "BA", Regiao.NORDESTE), 
	CE("Ceará", "CE", Regiao.NORDESTE), 
	DF("Distrito Federal", "DF", Regiao.CENTROOESTE), 
	ES("Espírito Santo", "ES", Regiao.SUDESTE), 
	GO("Goiás", "GO", Regiao.CENTROOESTE), 
	MA("Maranhão", "MA", Regiao.NORDESTE), 
	MT("Mato Grosso", "MT", Regiao.CENTROOESTE), 
	MS("Mato Grosso do Sul", "MS", Regiao.CENTROOESTE), 
	MG("Minas Gerais", "MG", Regiao.SUDESTE), 
	PA("Pará", "PA", Regiao.NORTE), 
	PB("Paraíba", "PB", Regiao.NORDESTE), 
	PR("Paraná", "PR", Regiao.SUL), 
	PE("Pernambuco", "PE", Regiao.NORDESTE), 
	PI("Piauí", "PI", Regiao.NORDESTE), 
	RJ("Rio de Janeiro", "RJ", Regiao.SUDESTE), 
	RN("Rio Grande do Norte", "RN", Regiao.NORDESTE), 
	RS("Rio Grande do Sul", "RS", Regiao.SUL), 
	RO("Rondônia", "RO", Regiao.NORTE), 
	RR("Roraima", "RR", Regiao.NORTE), 
	SC("Santa Catarina", "SC", Regiao.SUL), 
	SP("São Paulo", "SP", Regiao.SUDESTE), 
	SE("Sergipe", "SE", Regiao.NORDESTE), 
	TO("Tocantins", "TO", Regiao.NORTE);

	@Getter	private final String nome;
	@Getter	private final String sigla;
	@Getter private final Regiao regiao;

	private Estado(String nome, String sigla, Regiao regiao) {
		this.nome = nome;
		this.sigla = sigla;
		this.regiao = regiao;
	}
	
	public static Estado findBySigla(String sigla) {
		Estado result = null;
		
		if(sigla != null && !sigla.isEmpty()) {
			
			for(Estado e : values() ) {

				if (e.getSigla().equals(sigla)){
					result = e;
					break;
				} // if
				
			} // for
			
		} // if
			
		return result;
	} // findBySigla()
	
	public static List<Estado> findAllEstadosByRegiao(Regiao regiao) {
		List<Estado> result = null;

		if ( regiao != null ) {
			
			result = new ArrayList<>();
			
			for(Estado e : values() ) {
	
				if (e.getRegiao().equals(regiao)){
					result.add(e);
				} // if
				
			} // for
		} // if
			
		return result;
	} // findBySigla()

}
