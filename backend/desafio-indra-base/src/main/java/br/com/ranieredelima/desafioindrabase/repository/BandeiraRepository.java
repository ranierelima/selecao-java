package br.com.ranieredelima.desafioindrabase.repository;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.ranieredelima.desafioindrabase.model.Bandeira;

@Repository
public interface BandeiraRepository extends BaseRepository<Bandeira>{


	Bandeira findByNome(@Param("nome") String nome);

}
