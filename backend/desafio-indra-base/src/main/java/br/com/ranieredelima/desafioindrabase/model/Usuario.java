package br.com.ranieredelima.desafioindrabase.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.ranieredelima.desafioindrabase.model.generic.BaseModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "usuario")
@NoArgsConstructor
public class Usuario extends BaseModel{

	private static final long serialVersionUID = 1557242931151310558L;
	
	@Getter @Setter private String nome;
	
	@Getter(onMethod_ = { @Column( name="email", unique=true, nullable=false ) })
	@Setter private String email;
	
	@Getter(onMethod_= { @JsonIgnore }) @Setter private String senha;
	
}
