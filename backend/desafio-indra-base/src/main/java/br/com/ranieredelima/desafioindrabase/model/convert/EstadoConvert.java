package br.com.ranieredelima.desafioindrabase.model.convert;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import br.com.ranieredelima.desafioindrabase.model.Estado;

@Converter(autoApply = true)
public class EstadoConvert implements AttributeConverter<Estado, String>{

	@Override
	public String convertToDatabaseColumn(Estado estado) {
		return estado.name();
	}

	@Override
	public Estado convertToEntityAttribute(String dbData) {
		return Estado.valueOf(dbData);
	}

}
