package br.com.ranieredelima.desafioindrabase.model;

import java.util.Date;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.ranieredelima.desafioindrabase.dto.ValorMedioDTO;
import br.com.ranieredelima.desafioindrabase.model.generic.BaseModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "historico")
@NoArgsConstructor
@SqlResultSetMappings({
    @SqlResultSetMapping(name = "ValorMediaDTO", classes = {
		@ConstructorResult(targetClass = ValorMedioDTO.class, columns = {
			@ColumnResult(name = "valorCompra", type = Double.class),
			@ColumnResult(name = "valorVenda", type = Double.class)
		})
	}),
})
@NamedNativeQueries({
	@NamedNativeQuery(name = "Historico.getMediaPeloMunicipio",
		query = " SELECT avg(h.valor_compra) as valorCompra, avg(h.valor_venda) as valorVenda"
			+	" FROM historico h "
			+ 	" JOIN municipio m ON h.municipio_id = m.id "
			+	" WHERE m.nome = :municipio GROUP BY m.nome ",
		resultSetMapping = "ValorMediaDTO"),
	@NamedNativeQuery(name = "Historico.getMediaPelaBandeira",
		query = " SELECT avg(h.valor_compra) as valorCompra, avg(h.valor_venda) as valorVenda "
			+	" FROM historico h "
			+ 	" JOIN bandeira b ON h.bandeira_id = b.id "
			+	" WHERE b.nome = :bandeira GROUP BY b.nome ",
		resultSetMapping = "ValorMediaDTO"),
})
public class Historico extends BaseModel{
	
	private static final long serialVersionUID = 683381782816524400L;

	@Getter @Setter private String nomeEstabelecimento;
	
	@Getter @Setter private String produto;
	
	@Getter(onMethod_ = {@Temporal(TemporalType.DATE), @DateTimeFormat(pattern="yyyy/MM/dd")})
	@Setter private Date data;
	
	@Getter @Setter private Double valorCompra;
	
	@Getter @Setter private Double valorVenda;
	
	@Getter @Setter private String unidadeMedida;
	
	@Getter(onMethod_ = { @ManyToOne })  @Setter private Municipio municipio;
	
	@Getter(onMethod_ = { @ManyToOne })  @Setter private Bandeira bandeira;
	
} 
