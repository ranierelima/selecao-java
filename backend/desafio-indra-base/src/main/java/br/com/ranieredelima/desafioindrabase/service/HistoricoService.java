package br.com.ranieredelima.desafioindrabase.service;

import java.io.IOException;
import java.util.List;

import br.com.ranieredelima.desafioindrabase.dto.FileImportCSV;
import br.com.ranieredelima.desafioindrabase.dto.ValorMedioDTO;
import br.com.ranieredelima.desafioindrabase.model.Historico;
import br.com.ranieredelima.desafioindrabase.model.Regiao;

public interface HistoricoService extends BaseService<Historico>{

	Boolean importacaoArquivo(FileImportCSV fileImport) throws IOException;

	List<Object[]> obterMediaPrecoCombustivelPeloMunicipio(String municipio);

	List<Historico> obterDadosPelaRegiao(Regiao regiaoEnum);
	
	ValorMedioDTO getMediaPeloMunicipio(String municipio);

	ValorMedioDTO getMediaPelaBandeira(String bandeira);

}
