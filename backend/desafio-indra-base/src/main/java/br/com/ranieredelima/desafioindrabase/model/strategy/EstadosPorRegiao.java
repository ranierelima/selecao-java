package br.com.ranieredelima.desafioindrabase.model.strategy;

import java.util.List;

import br.com.ranieredelima.desafioindrabase.model.Estado;

public interface EstadosPorRegiao {

	List<Estado> findAllEstados();
	
}
