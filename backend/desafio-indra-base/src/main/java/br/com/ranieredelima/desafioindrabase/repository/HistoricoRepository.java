package br.com.ranieredelima.desafioindrabase.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.ranieredelima.desafioindrabase.dto.ValorMedioDTO;
import br.com.ranieredelima.desafioindrabase.model.Estado;
import br.com.ranieredelima.desafioindrabase.model.Historico;

@Repository
public interface HistoricoRepository extends BaseRepository<Historico>{
	
	@Query(value="SELECT h.produto, avg(h.valorVenda) FROM Historico h WHERE h.municipio.nome = :municipio GROUP BY h.produto")
	public List<Object[]> obterMediaPrecoPeloMunicipio(@Param("municipio") String municipio);

	@Query(value="SELECT h FROM Historico h WHERE h.municipio.estado IN ( :estados ) ")
	public List<Historico> obterDadosPelaRegiao(@Param("estados") List<Estado> estados);

	@Query(nativeQuery = true)
	public ValorMedioDTO getMediaPeloMunicipio(@Param("municipio") String municipio);

	@Query(nativeQuery = true)
	public ValorMedioDTO getMediaPelaBandeira(@Param("bandeira") String bandeira);

}
