package br.com.ranieredelima.desafioindrabase.service;

import br.com.ranieredelima.desafioindrabase.model.Bandeira;

public interface BandeiraService extends BaseService<Bandeira>{

	Bandeira findByNome(String nome);

}
