package br.com.ranieredelima.desafioindrabase.configuration;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration("AutomaticConfiguration")
@ComponentScan("br.com.ranieredelima.desafioindrabase")
@EntityScan("br.com.ranieredelima.desafioindrabase")
@EnableJpaRepositories("br.com.ranieredelima.desafioindrabase")
public class AutomaticConfiguration {

}
