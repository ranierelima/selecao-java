package br.com.ranieredelima.desafioindrabase.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ranieredelima.desafioindrabase.model.Usuario;
import br.com.ranieredelima.desafioindrabase.repository.UsuarioRepository;
import br.com.ranieredelima.desafioindrabase.service.UsuarioService;

@Service
public class UsuarioServiceImpl extends BaseServiceImpl<Usuario> implements UsuarioService{

	@Autowired
	public UsuarioServiceImpl(UsuarioRepository repository) {
		super(repository);
	}

}
