package br.com.ranieredelima.desafioindrabase.model;

import java.util.List;

import br.com.ranieredelima.desafioindrabase.model.strategy.EstadosPorRegiao;
import lombok.Getter;

public enum Regiao implements EstadosPorRegiao{

	CENTROOESTE("CO","Centro Oeste"){
		
		@Override
		public List<Estado> findAllEstados() {
			return Estado.findAllEstadosByRegiao(CENTROOESTE);
		}
		
	},
	NORDESTE("NE", "Nordeste"){
		
		@Override
		public List<Estado> findAllEstados() {
			return Estado.findAllEstadosByRegiao(NORDESTE);
		}
		
	},
	NORTE("N", "Norte"){
		
		@Override
		public List<Estado> findAllEstados() {
			return Estado.findAllEstadosByRegiao(NORTE);
		}
		
	},
	SUDESTE("SE", "Sudeste"){
		
		@Override
		public List<Estado> findAllEstados() {
			return Estado.findAllEstadosByRegiao(SUDESTE);
		}
		
	},
	SUL("S", "Sul"){
		
		@Override
		public List<Estado> findAllEstados() {
			return Estado.findAllEstadosByRegiao(SUL);
		}
		
	};

	@Getter private final String sigla;
	@Getter private final String nome;
	
	private Regiao(String sigla, String nome){
		this.sigla = sigla;
		this.nome = nome;
	} // Regiao()
	
	public static Regiao findBySigla(String sigla) {
		Regiao result = null;
		
		for(Regiao r : values() ) {

			if (r.getSigla().equals(sigla)){
				result = r;
				break;
			} // if
			
		} // for
			
		return result;
	} // findBySigla()
	
}
