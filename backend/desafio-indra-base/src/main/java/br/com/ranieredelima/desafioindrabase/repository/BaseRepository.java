package br.com.ranieredelima.desafioindrabase.repository;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

import br.com.ranieredelima.desafioindrabase.model.generic.BaseModel;


@NoRepositoryBean
public interface BaseRepository<T extends BaseModel> extends PagingAndSortingRepository<T, Long> {
	
	
}

