package br.com.ranieredelima.desafioindrabase.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class ValorMedioDTO {

	@Getter @Setter private Double valorCompra;
	@Getter @Setter private Double valorVenda;
}
